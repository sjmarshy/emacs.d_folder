;; Defaults for emacs
;; Not using any 3rd party plugins
;; Aaaaall emacs.

(setq-default
 inhibit-splash-screen t
 truncate-lines t
 require-final-newline 'visit-save)

(global-hl-line-mode t)
(set-face-background 'hl-line "sky blue")
(set-face-foreground 'hl-line "DodgerBlue4")

(which-func-mode t)
(transient-mark-mode t)
(global-font-lock-mode t)
(require 'uniquify)

(require 'saveplace)
(setq-default save-place t)
(setq-default save-place-file "~/.emacs.d/saved-places")

(provide 'defaults)
