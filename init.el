(add-to-list 'load-path "~/.emacs.d/config")
(add-to-list 'load-path "~/.emacs.d/lib")  ;; No longer have libraries directly in the .emacs.d folder.

(setq
 main-dir   (file-name-as-directory "~/.emacs.d")
 config-dir (file-name-as-directory (concat main-dir "config"))
 lib-dir    (file-name-as-directory (concat main-dir "lib"))
 )

(require 'defaults)



;; Key Chord Mode
(require 'key-chord)
(key-chord-mode 1)
(key-chord-define-global ";;" "\C-e;");
(key-chord-define-global "fg" 'isearch-forward)
(key-chord-define-global "df" 'isearch-backward)


;; Here should be all the `if emacs is more than 24` stuff...it'll go here eventually
;; package archives - save for emacs > 24


(when (> emacs-major-version 23)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
  (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/") t))

;; Setting File Associations
(setq auto-mode-alist (cons '("\\.js$" . js-mode) auto-mode-alist))

;; Load and configure packages:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Autocomplete:
(add-to-list 'load-path "~/.emacs.d/lib/autocomplete")
(require 'auto-complete-config)
(ac-config-default)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/lib/autocomplete/ac-dict")

;; Anything:
(add-to-list 'load-path "~/.emacs.d/lib/anything/")
(require 'anything)
(require 'anything-match-plugin)
(require 'anything-config)
(global-set-key (kbd "C-S-s") 'anything)

;; Word-Count:
(require 'word-count)

;; Web Mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ctp\\'" . web-mode))

(defun web-mode-hook()
  (auto-complete-mode 1)
  (zencoding-mode 1)
  (electric-pair-mode 1))

(add-hook 'web-mode-hook 'web-mode-hook)


(add-to-list 'load-path "~/.emacs.d/lib/sass-mode")
(add-to-list 'load-path "~/.emacs.d/lib/haml-mode")
(require 'sass-mode)
(add-to-list 'auto-mode-alist '("\\.sass\\'" . sass-mode))
(defun sass-mode-hook()
  (auto-complete-mode 1))
(add-hook 'sass-mode-hook 'sass-mode-hook)

;; ZenCoding:
(require 'zencoding-mode)

;; Powerline:
(add-to-list 'load-path "~/.emacs.d/lib/powerline")
(require 'powerline)
(setq powerline-arrow-shape 'arrow)
(setq powerline-color1 "grey22")
(setq powerline-color2 "grey40")
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "outline" :family "Droid Sans Mono"))))
 '(erc-input-face ((t (:foreground "dodger blue"))))
 '(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face))))
 '(font-lock-comment-face ((t (:foreground "#cc0000"))))
 '(font-lock-keyword-face ((t (:foreground "red4"))))
 '(mode-line ((t (:foreground "#030303" :background "#bdbdbd" :box nil))))
 '(mode-line-interactive ((t (:foreground "#f9f9f9" :background "#666666" :box nil))))
 '(show-paren-match ((t (:background "dark green" :foreground "snow")))))

;; Colour Theme:
 (add-to-list 'load-path "~/.emacs.d/lib/colour-theme")
(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-whateveryouwant)))

;; YaSnippet
(add-to-list 'load-path "~/.emacs.d/lib/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)
(setq yas/root-directory "~/.emacs.d/lib/snippets")
(yas/load-directory yas/root-directory)

;; Breadcrumb
(require 'breadcrumb)
(global-set-key "\C-c\ s"  'bc-set)
(global-set-key "\C-c\ j"  'bc-next)
(global-set-key "\C-c\M-j"  'bc-previous)
(global-set-key "\C-c\ f"  'bc-local-next)
(global-set-key "\C-c\ b"  'bc-local-previous)
(global-set-key "\C-c\ l"  'bc-list)

;; Autopair:
(defun autopair-setup ()
  (require 'autopair)
  (autopair-global-mode 1))

(if (>= emacs-major-version 24)
    (electric-pair-mode 1)
  (autopair-setup))
(show-paren-mode 1)
(global-linum-mode 1)

(setq linum-format "%d ")

;; Need to add this custom code into it's own directory
;; will now prompt you to make a directory before you save. 
(add-hook 'before-save-hook
	  (lambda ()
	    (when buffer-file-name
	      (let ((dir (file-name-directory buffer-file-name)))
		(when (and (not (file-exists-p dir))
			   (y-or-n-p (format "Directory %s does not exist. Create it?" dir)))
		  (make-directory dir t))))))

;keymaps
(define-key global-map (kbd "RET") 'newline-and-indent)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key (kbd "C-q") 'kill-whole-line)

(set-register ?e '(file . "~/.emacs.d/lib/init.el"))

;; Autocomplete new key bindings
(define-key ac-completing-map "\C-m" nil)
(define-key ac-completing-map (kbd "C-<tab>") 'ac-complete)
(setq ac--use-menu-map t)
(define-key ac-menu-map "\C-m" 'ac-complete)

;; Keymap C-c C-m to deactivate the mark.
(defun my-deactivate-mark ()
  (interactive)
  (setq deactivate-mark 1))
(global-set-key "\C-c\C-m" 'my-deactivate-mark)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

;; Mark Current Word code
(defun my-mark-current-word (&optional arg allow-extend)
  "Put point at the beginning of current word, mark at end."
  (interactive "p\np")
  (setq arg (if arg arg 1))
  (if (and allow-extend
	    (or (and (eq last-command this-command) (mark t))
		(region-active-p)))
       (set-mark
	(save-excursion
	  (when (< (mark)(point))
	    (setq arg (- arg)))
	  (goto-char (mark))
	  (forward-word arg)
	  (point)))
     (let ((wbounds (bounds-of-thing-at-point 'word)))
       (unless (consp wbounds)
	 (error "No Word at Point."))
       (if (>= arg 0)
	   (goto-char (car wbounds))
	 (goto-char (cdr wbounds)))
       (push-mark (save-excursion
		    (forward-word arg)
		    (point)))
       (activate-mark))))
(global-set-key "\C-c\C-d" 'my-mark-current-word)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-directory-alist nil)
 '(erc-nick "sjmarshy")
 '(font-lock-global-modes (quote (not speedbar-mode)))
 '(midnight-mode t nil (midnight))
 '(version-control t)
 '(yas-prompt-functions (quote (yas-ido-prompt yas-completing-prompt yas-no-prompt))))

(put 'narrow-to-region 'disabled nil)

;; Open line ammendment
(defun open-next-line (arg)
  "Move to the next line and then opens a line."
  (interactive "p")
  (newline-and-indent)
  (previous-line)
  (end-of-line)
  (newline-and-indent))
(global-set-key (kbd "C-o") 'open-next-line)
(global-set-key (kbd "C-:") 'goto-line)

(require 'go-mode)
(put 'upcase-region 'disabled nil)

(ido-mode 1)

(set-face-attribute 'font-lock-comment-face nil :font "Anonymous Pro Minus-10")
(set-face-attribute 'default nil :font "Anonymous Pro Minus-10")
(set-face-attribute 'font-lock-keyword-face nil :weight 'regular)
(set-face-attribute 'font-lock-function-name-face nil :weight 'regular)
(set-face-attribute 'font-lock-type-face nil :weight 'normal :foreground "LightSlateGrey")
(set-face-attribute 'erc-input-face nil :foreground "LightSlateGrey")

;;(set-frame-parameter (selected-frame) 'alpha '(<active> [<inactive>]))
(set-frame-parameter (selected-frame) 'alpha '(92 80))
(add-to-list 'default-frame-alist '(alpha 92 80))

(setq make-backup-files nil)
(setq auto-save-default nil)

(eval-after-load 'sws-mode
  '(define-key sws-mode-map [(tab)] 'sws-at-bol-p))
